import jwt
from django.db import models
from django.conf import settings
from datetime import datetime, timedelta
from django.contrib.auth.models import (BaseUserManager, PermissionsMixin,
                                        AbstractBaseUser)
from core.models import TimestampedModel


class UserManager(BaseUserManager):
    def create_user(self, username, email, password, **extra_kwargs):
        """Creates and saves user model with given username, password, email"""

        if not username:
            raise ValueError('User must have username')

        if not email:
            raise ValueError("User must have email")
        email = self.normalize_email(email)
        user = self.model(username=username, email=email, **extra_kwargs)
        user.set_password(password)
        user.save(self._db)

        return user

    def create_superuser(self, username, email, password=None, **extra_kwargs):
        if password is not None:
            raise TypeError("Superusers must have a password")

            user = create_user(username, email, password)
            user.is_staff = True
            user.is_super = True
            user.save()

            return user


class User(AbstractBaseUser, PermissionsMixin, TimestampedModel):
    username = models.CharField(db_index=True, max_length=255, unique=True)
    email = models.EmailField(db_index=True, unique=True)

    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def full_name(self):
        self.username

    def __repr__(self):
        return 'User({0.email})'.format(self)

    @property
    def token(self):
        return self._generate_jwt_token()

    def _generate_jwt_token(self):
        dt = datetime.now() + timedelta(days=60)

        token = jwt.encode({
            'id': self.pk,
            'exp': int(dt.strftime('%s'))
        },
                           settings.SECRET_KEY,
                           algorithm='HS256')

        return token.decode('utf-8')
