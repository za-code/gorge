from rest_framework import serializers
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate

from Profile.serializers import ProfileSerializer


class RegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()

        fields = ('email', 'username', 'password', 'token')
        extra_kwargs = {'password': {'write_only': True, 'min_length': 5}}

    def create(self, validated_data):
        return get_user_model().objects.create_user(**validated_data)


class LoginSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=255)
    username = serializers.CharField(max_length=255, read_only=True)
    password = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)

    def validate(self, data):
        email = data.get('email', None)
        password = data.get('password', None)

        if not email:
            raise serializers.ValidationError(
                'An email address is required to login')

        if not password:
            raise serializers.ValidationError('Password is required to login')

        user = authenticate(email=email, password=password)

        if user is None:
            raise serializers.ValidationError(
                "User with this email & password was not found!")

        return {
            'email': user.email,
            'username': user.username,
            'token': user.token
        }


class UserSerializer(serializers.ModelSerializer):
    """Handles serialization & deserialization of User objects"""

    password = serializers.CharField(
        max_length=128, min_length=8, write_only=True)

    profile = ProfileSerializer(write_only=True)
    bio = serializers.CharField(source='profile.bio')
    image = serializers.CharField(source='profile.image')

    class Meta:
        model = get_user_model()
        fields = (
            'email',
            'username',
            'password',
            'profile',
            'bio',
            'image',
        )

        read_only_fields = ('token', )

    def update(self, instance, validated_data):

        password = validated_data.pop('password', None)
        profile_data = validated_data.pop('profile', {})

        for (key, value) in validated_data.items():
            setattr(instance, key, value)

        if password is not None:
            instance.set_password(password)

        instance.save()

        for (key, value) in profile_data.items():
            print(instance.profile)
            setattr(instance.profile, key, value)

        instance.profile.save()

        return instance
