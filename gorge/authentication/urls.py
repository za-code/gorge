from django.urls import path
from authentication import views

urlpatterns = [
    path('signup/', views.RegistrationView.as_view(), name='signup'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('user/', views.UserRetrieveUpdateAPIView.as_view(), name='user-detail')
]