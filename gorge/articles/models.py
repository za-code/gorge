from django.db import models
from django.utils.text import slugify


class Article(models.Model):
    slug = models.SlugField(db_index=True, max_length=255, unique=True)
    title = models.CharField(db_index=True, max_length=255)
    description = models.TextField()
    body = models.TextField()
    author = models.ForeignKey('Profile.Profile', on_delete=models.CASCADE)
    tags = models.ManyToManyField('articles.Tag', related_name='articles')

    def __str__(self):
        return self.title

    @classmethod
    def _generate_slug(cls, field):
        origin_slug = slugify(field)
        unique_slug = origin_slug
        identifier = 1

        while cls.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(origin_slug, identifier)
            identifier += 1
        return unique_slug

    def save(self, *args, **kwargs):
        self.slug = self._generate_slug(self.title)
        super().save(*args, **kwargs)


class Tag(models.Model):
    tag = models.CharField(max_length=255)
    slug = models.SlugField(db_index=True, unique=True)

    def __str__(self):
        return self.tag


class Comment(models.Model):
    body = models.TextField()
    article = models.ForeignKey(
        'articles.Article', related_name='comments', on_delete=models.CASCADE)
    author = models.ForeignKey(
        'Profile.Profile', related_name='comments', on_delete=models.CASCADE)
