from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from articles.serializer import ArticleSerializer, CommentSerializer

from articles.models import Article
from Profile.models import Profile


class ArticleAPIView(generics.CreateAPIView, generics.UpdateAPIView,
                     generics.ListAPIView, generics.DestroyAPIView):
    serializer_class = ArticleSerializer
    queryset = Article.objects.select_related('author', 'author__user')
    queryset = Article.objects.all()
    permission_classes = (IsAuthenticated, )
    lookup_field = 'slug'

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = self.queryset.get(slug=self.kwargs[self.lookup_field])
        return obj

    # def get_queryset(self):
    #     queryset = self.queryset

    #     author = self.request.query_params('author', None)
    #     if author is not None:
    #         queryset = queryset.filter(author__user__username=author)

    #     tag = self.request.query_params.get('tag', None)
    #     if tag is not None:
    #         queryset = queryset.filter(tags__tag=tag)
    #     return queryset

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data, context={'author': request.user.profile})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.serializer_class(
            instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        message = {"message": "Successfully deleted..."}
        return Response(message)


class CommentAPIView(APIView):
    serializer_class = CommentSerializer
    permission_classes = (IsAuthenticated, )

    # def get_object(self):

    def post(self, request, *args, **kwargs):
        article = Article.objects.get(slug=kwargs['slug'])
        author = Profile.objects.get(user=request.user)
        serializer = self.serializer_class(
            data=request.data, context={
                'article': article,
                'author': author
            })
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status.HTTP_201_CREATED)
