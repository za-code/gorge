from django.urls import path, include
from articles.views import ArticleAPIView, CommentAPIView
from rest_framework.routers import DefaultRouter

# router = DefaultRouter(trailing_slash=False)
# router.register(r'articles', ArticleAPIView)

urlpatterns = [
    path('article/', ArticleAPIView.as_view(), name='article'),
    path(
        'article/<slug:slug>', ArticleAPIView.as_view(),
        name='article-update'),
    path(
        'article/<slug:slug>/comment',
        CommentAPIView.as_view(),
        name='comment')
]
