from rest_framework import serializers
from Profile.serializers import ProfileSerializer
from .relations import TagRelatedField
from articles.models import Article, Tag, Comment


class ArticleSerializer(serializers.ModelSerializer):
    author = ProfileSerializer(read_only=True)
    description = serializers.CharField(required=False)
    slug = serializers.SlugField(required=False)
    tagList = TagRelatedField(many=True, required=False, source='tags')

    class Meta:
        model = Article
        fields = ('title', 'slug', 'description', 'author', 'tagList')

    def create(self, validated_data):
        author = self.context.get('author', None)
        tags = validated_data.pop('tags', [])

        article = Article.objects.create(author=author, **validated_data)

        for tag in tags:
            article.tags.add(tag)
        return article

    def update(self, instance, validated_data):
        for (key, value) in validated_data.items():
            setattr(instance, key, value)

        instance.save()
        return instance


class CommentSerializer(serializers.ModelSerializer):
    article = ArticleSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = ('id', 'article', 'body')

    def create(self, validated_data):
        article = self.context.get('article')
        author = self.context.get('author')
        return Comment.objects.create(
            article=article, author=author, **validated_data)


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('tag', )

    def to_representation(self, obj):
        return obj.tag