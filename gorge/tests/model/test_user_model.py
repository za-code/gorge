from django.test import TestCase
from django.contrib.auth import get_user_model
from Profile.models import Profile


class UserModel(TestCase):
    def create_user(self):
        user = {
            "username": "am_test",
            "email": "testme@email.com",
            "password": "HahahaPassMe!"
        }

        return get_user_model().objects.create_user(**user)

    def test_user_model_string_representation(self):
        """Test string representation when user object is created"""
        user = self.create_user()
        self.assertEqual(str(user), user.email)

    def test_profile_is_created_when_user_object_is_created(self):
        """Test profile object is created when user object is created"""
        user = self.create_user()
        self.assertTrue(Profile.objects.count(), 1)