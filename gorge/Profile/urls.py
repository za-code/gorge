from django.urls import path
from Profile.views import ProfileAPIView

urlpatterns = [
    path('profile/<str:username>/', ProfileAPIView.as_view(), name='profile')
]