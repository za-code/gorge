from rest_framework import generics, mixins
from rest_framework.views import APIView
from rest_framework.exceptions import NotFound
from Profile.serializers import ProfileSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from .models import Profile


class ProfileAPIView(generics.RetrieveAPIView):
    serializer_class = ProfileSerializer
    queryset = Profile.objects.select_related('user')
    permission_classes = (IsAuthenticated, )

    def retrieve(self, request, username, *args, **kwargs):
        try:
            profile = self.queryset.get(user__username=username)
        except Profile.DoesNotExist:
            raise NotFound("A profile with this username does not exist")
        serializer = self.serializer_class(profile)
        return Response(serializer.data, status=status.HTTP_200_OK)
