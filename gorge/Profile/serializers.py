from rest_framework import serializers
from django.contrib.auth import get_user_model
from Profile.models import Profile


class ProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username', read_only=True)
    image = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = (
            'username',
            'bio',
            'image',
        )

    def get_image(self, obj):
        if obj.image:
            return obj.image

        return 'https://static.productionready.io/images/smiley-cyrus.jpg'
