from django.db import models
from django.contrib.auth import get_user_model
from core.models import TimestampedModel


class Profile(TimestampedModel):
    bio = models.TextField(blank=True)
    image = models.ImageField(blank=True)
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)


def __repr__(self):
    return "Profile({0.user.username})".format(self)


def __str__(self):
    return f"{self.user.username}"
