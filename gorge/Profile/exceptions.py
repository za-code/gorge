from rest_framework.exceptions import APIException
from rest_framework import status


class ProfileDoesNotExist(APIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = 'The requested_profile does not exist'
